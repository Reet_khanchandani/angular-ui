
import { Component,Input, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { Stocks } from '../shared/stocks';
import { ActivatedRoute} from '@angular/router';

interface Animal {
  name: string;
  sound: string;
}


@Component({
  selector: 'app-dashboardf',
  templateUrl: './dashboardf.component.html',
  styleUrls: ['./dashboardf.component.scss']
})


export class DashboardfComponent implements OnInit {
  name = 'Angular 5';
  

  Stocks: any = [];

  selectFormControl = new FormControl('', Validators.required);

  @Input() stockdetails = { id:0, datetime:"",stockTicker: "", price: 0 , volume: 0, buyOrSell:"",statusCode:0 }

  constructor(
    private route: ActivatedRoute,
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getAllStocks().subscribe((data: {}) => {
        this.Stocks = data;
    })
  }

  getStockById(id: number){
    this.router.navigate(['stock-status',id])
  }


  addStocks() {
    this.restApi.addStocks(this.stockdetails).subscribe((data: {}) => {
      this.router.navigate(['/cart'])
    })
  }
}

// export interface Element {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }

// const ELEMENT_DATA: Element[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'}
// ];
