import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute} from '@angular/router';
import { Stocks } from '../shared/stocks';

@Component({
  selector: 'app-stockdetails',
  templateUrl: './stockdetails.component.html',
  styleUrls: ['./stockdetails.component.scss']
})
export class StockdetailsComponent implements OnInit {

  id: any;

  Stocks: any = [];
  constructor(public restApi: RestApiService,
    private route: ActivatedRoute) { }


    
  ngOnInit(): void {
    this.id = this.route.snapshot.params["id"];

    this.restApi.getStocksById(this.id).subscribe((data: {}) => {
      this.Stocks = data;
  });

}
}
