import { Component,Input, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { Stocks } from '../shared/stocks';
import { ActivatedRoute} from '@angular/router';

export class savedData {
  constructor(
    public symbol: string,
    public nse: number,
    public bse:number,
    public difference:number,
    public noOfStocks:number,
    public Profit:number,
    public like:number,
    public date:string
  ){

  }
}
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent implements OnInit {
  name = 'Angular 5';
  

  Stocks: any = [];

  selectFormControl = new FormControl('', Validators.required);

  @Input() stockdetails = { id:0, datetime:"",stockTicker: "", price: 0 , volume: 0, buyOrSell:"",statusCode:0 }

  constructor(
    private route: ActivatedRoute,
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getAllStocks().subscribe((data: {}) => {
        this.Stocks = data;
    })
  }

  getStockById(id: number){
    this.router.navigate(['stock-status',id])
  }


  
}

